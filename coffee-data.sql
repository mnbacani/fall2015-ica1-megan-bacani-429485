mysql> create DATABASE coffee;
Query OK, 1 row affected (0.00 sec)


mysql> CREATE TABLE coffees(
    -> id mediumint unsigned auto_increment,
    -> name varchar(50),
    -> primary key (id)
    -> )engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
Query OK, 0 rows affected (0.01 sec)

mysql> CREATE TABLE ratings( coffeeId mediumint unsigned auto_increment,  user varchar(50) not null,  rating Decimal(1,1) not null,  comment tinytext, primary key (coffeeId), foreign key (coffeeId) references coffees(id))engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
Query OK, 0 rows affected (0.00 sec)